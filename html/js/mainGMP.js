var pose, poseAlt, min, max,
    randVar, falscheAntwort, btn1, btn2, arrLength, falscheAntwort, arrObj, objPose, strgPose, objPoseFalsch, strgPoseFalsch, posenString = "",
    stringFalsch, points, bgImage, position, counter, frageURL;

var arrPosen = posen;



function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getPose(pose_alt) {
    arrLength = arrPosen.length;
    randVar = getRandomIntInclusive(0, arrLength - 1);
    objPose = arrPosen[randVar];
    strgPose = objPose.pose;
    if (strgPose != pose_alt) {
        pose = strgPose;
        //        pose_alt = pose;
        return pose;
    } else {
        if (randVar >= 1) {
            objPose = arrPosen[randVar - 1];
            strgPose = objPose.pose;
            pose = strgPose;
            //            pose_alt = pose;
            return pose;
        } else {
            objPose = arrPosen[randVar + 1];
            strgPose = objPose.pose;
            pose = strgPose;
            //            pose_alt = pose;
            return pose;
        }
    }
}


function getFalscheAntwort(poseRichtig) {
    arrLength = arrPosen.length;
    randVar = getRandomIntInclusive(0, arrLength - 1);
    objPoseFalsch = arrPosen[randVar];
    strgPoseFalsch = objPoseFalsch.pose;
    if (strgPoseFalsch != poseRichtig) {
        falscheAntwort = strgPoseFalsch;
        return falscheAntwort;
    } else {
        if (randVar >= 1) {
            objPoseFalsch = arrPosen[randVar - 1];
            strgPoseFalsch = objPoseFalsch.pose;
            falscheAntwort = strgPoseFalsch;
            return falscheAntwort;
        } else {
            objPoseFalsch = arrPosen[randVar + 1];
            strgPoseFalsch = objPoseFalsch.pose;
            falscheAntwort = strgPoseFalsch;
            return falscheAntwort;
        }
    }
}




//function button_click(id) {
//    session.service("ALMemory").then(function (memory) {
//        if (id == "btnJa") {
//            document.getElementById('choice').style.visibility = "hidden";
//            points = 0;
//            counter = 1;
//            getPose();
//            bgImage = "../images/ht/background/" + pose + ".jpg";
//            frageURL = "frage.html?variable=" + pose
//            memory.raiseEvent("bgImage", bgImage);
//            memory.raiseEvent("posenname", pose);
//            memory.raiseEvent("frageURL", frageURL);
//            //console.log("buttonJa");
//            memory.raiseEvent("varPunkte", points);
//            memory.raiseEvent("btnJa", 1);
//        }
//        if (id == "btnNein") {
//            memory.raiseEvent("btnNein", 1);
//        }
//        if (id == "restart") {
//            memory.raiseEvent("onStart", 1);
//        }
//        if (id == "end") {
//            memory.raiseEvent("onEnd", 1);
//        }
//        if (id == "button1") {
//            //            counter += 1;
//            //            if (counter <= 3) {
//            getPose();
//            frageURL = "frage.html?variable=" + pose
//            bgImage = "../images/ht/background/" + pose + ".jpg";
//            var btnSRC = document.getElementById('btn1').src;
//            if (btnSRC == posenString) {
//                //                points = memory.getData("varPunkte") + 1;
//                //                memory.raiseEvent("varPunkte", points);
//                memory.raiseEvent("bgImage", bgImage);
//                memory.raiseEvent("posenname", pose);
//                memory.raiseEvent("frageURL", frageURL);
//                memory.raiseEvent("onPoseRichtig", 1);
//                console.log("button1 richtig");
//            } else if (btnSRC == stringFalsch) {
//                memory.raiseEvent("bgImage", bgImage);
//                memory.raiseEvent("posenname", pose);
//                memory.raiseEvent("frageURL", frageURL);
//                memory.raiseEvent("onPoseFalsch", 1);
//                console.log("button1 falsch");
//            }
//            //            } else {
//            //                var btnSRC = document.getElementById('btn1').src;
//            //                if (btnSRC == posenString) {
//            //                    points = memory.getData("varPunkte") + 1;
//            //                    memory.raiseEvent("varPunkte", points);
//            //                    memory.raiseEvent("onLetztePoseRichtig", 1);
//            //                    console.log("button1 richtig");
//            //                } else if (btnSRC == stringFalsch) {
//            //                    points = memory.getData("varPunkte");
//            //                    memory.raiseEvent("varPunkte", points);
//            //                    memory.raiseEvent("onLetztPoseFalsch", 1);
//            //                    console.log("button1 falsch");
//            //                }
//            //            }
//        }
//        if (id == "button2") {
//            //            counter += 1;
//            //            if (counter <= 3) {
//            getPose();
//            frageURL = "frage.html?variable=" + pose
//            bgImage = "../images/ht/background/" + pose + ".jpg";
//            var btnSRC = document.getElementById('btn2').src;
//            if (btnSRC == posenString) {
//                //                points = memory.getData("varPunkte") + 1;
//                //                memory.raiseEvent("varPunkte", points);
//                memory.raiseEvent("bgImage", bgImage);
//                memory.raiseEvent("posenname", pose);
//                memory.raiseEvent("frageURL", frageURL);
//                memory.raiseEvent("onPoseRichtig", 1);
//                console.log("button2 richtig");
//            } else if (btnSRC == stringFalsch) {
//                memory.raiseEvent("bgImage", bgImage);
//                memory.raiseEvent("posenname", pose);
//                memory.raiseEvent("frageURL", frageURL);
//                memory.raiseEvent("onPoseFalsch", 1);
//                console.log("button2 falsch");
//            }
//            //            } else {
//            //                var btnSRC = document.getElementById('btn2').src;
//            //                if (btnSRC == string) {
//            //                    points = memory.getData("varPunkte") + 1;
//            //                    memory.raiseEvent("varPunkte", points);
//            //                    memory.raiseEvent("onLetztePoseRichtig", 1);
//            //                    console.log("button1 richtig");
//            //                } else if (btnSRC == stringFalsch) {
//            //                    points = memory.getData("varPunkte");
//            //                    memory.raiseEvent("varPunkte", points)
//            //                    memory.raiseEvent("onLetztPoseFalsch", 1);
//            //                    console.log("button1 falsch");
//            //                }
//            //            }
//        } else memory.raiseEvent(id, 1).then(function () {
//            console.log("no button found");
//        });
//    });
//}

function getBtnPosition(poseRichtig) {
    //    console.log("Position");
    //    session.service("ALMemory").then(function (memory) {
    //    pose = memory.getData("namePose");
    //    });
    position = 'btn' + getRandomIntInclusive(1, 2);
    posenString = "./images/ht/posen/" + poseRichtig + ".jpg";
    //    bgImage = "./images/ht/background/" + poseRichtig + ".jpg";
    console.log("posenString: " + posenString);
    document.getElementById(position).src = posenString;
    if (position == "btn1") {
        stringFalsch = "./images/ht/posen/" + getFalscheAntwort(poseRichtig) + ".jpg";
        document.getElementById('btn2').src = stringFalsch;
    } else {
        stringFalsch = "./images/ht/posen/" + getFalscheAntwort(poseRichtig) + ".jpg";
        document.getElementById('btn1').src = stringFalsch;
    }
    return posenString, stringFalsch;
}


function presentResult() {
    session.service("ALMemory").then(function (memory) {
        var endPoints = memory.getData("varPunkte");
        if (endPoints == 0) {
            memory.raiseEvent("nullPunkte", 1);
        }
        if (endPoints == 1) {
            memory.raiseEvent("einPunkt", 1);
        }
        if (endPoints == 2) {
            memory.raiseEvent("zweiPunkte", 1);
        }
        if (endPoints == 3) {
            memory.raiseEvent("dreiPunkte", 1);
        } else memory.raiseEvent(endPoints, 1).then(function () {
            console.log("home send");
        });
    });
}

function clearField(input) {
    input.value = "";
}

function disconnected(error) {
    console.log("disconnected");
}
