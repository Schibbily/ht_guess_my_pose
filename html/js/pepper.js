var textToSpeech = null;
var memory = null;
var session = new QiSession(function (session) {
    console.log("connected!");
    // you can now use your QiSession
}, function () {
    console.log("disconnected");
});
if (typeof QiSession !== 'undefined') {
    QiSession(function (session) {
        console.log("connected!");
        session.service("ALAnimatedSpeech").then(function (tts) {
            textToSpeech = tts;
        }, function (error) {
            console.log("An error occurred:", error);
        });
        session.service("ALMemory").then(function (ALMemory) {
            memory = ALMemory;
            memory.subscriber("searchReceived").then(function (subscriber) {
                subscriber.signal.connect(function (search) {
                    console.log("search event: " + search);
                    performVoiceSearch(search);
                });
            });
        });
    }, function () {
        console.log("disconnected");
    });
}

function saySentence(sentence) {
    if (sentence) {
        console.log("say: " + sentence);
        if (textToSpeech) {
            memory.raiseEvent("onSendSpeechToRobot", sentence);
        }
    }
}

function stopSpeech() {
    textToSpeech._stopAll(true);
}
$(document).ready(function () {
    $("#new-search").click(function () {
        stopSpeech();
        //goBack();
    });
    $("#customerNameButton").click(function () {
        stopSpeech();
        //goBack();
    });
});