var aTextToSpeech = null;
var memory = null;
var pose;
session = null;
QiSession(connected, disconnected, location.host);
//using qimessaging v2

function getBtnPosition() {
    //    console.log("Position");
    session.service("ALMemory").then(function (memory) {
        pose = memory.getData("namePose");
    });
    var position = "btn" + getRandomIntInclusive(1, 2);
    var string = "./images/ht/posen/" + pose + ".jpg";
    var bgImage = "./images/ht/background/" + pose + ".jpg";
    document.getElementById(position).src = string;
    if (position == "btn1") {
        stringFalsch = "./images/ht/posen/" + getFalscheAntwort(pose) + ".jpg";
        document.getElementById('btn2').src = stringFalsch;
    } else {
        stringFalsch = "./images/ht/posen/" + getFalscheAntwort(pose) + ".jpg";
        document.getElementById('btn1').src = stringFalsch;
    }
}

function connected(s) {
    console.log("connected");
    session = s
    console.log(s);
    session.service("ALAnimatedSpeech").then(function (atts) {
        aTextToSpeech = atts;
    });
    session.service("ALMemory").then(function (ALMemory) {
        memory = ALMemory;
        getBtnPosition();
    });
}

function disconnected(error) {
    console.log("disconnected");
}
QiSession(connected, disconnected, location.host);
