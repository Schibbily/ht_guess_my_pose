var aTextToSpeech = null;
var memory = null;
session = null;
QiSession(connected, disconnected, location.host);
//using qimessaging v2
function connected(s) {
    console.log("connected");
    session = s
    session.service("ALAnimatedSpeech").then(function (atts) {
        aTextToSpeech = atts;
    });
    session.service("ALMemory").then(function (ALMemory) {
        memory = ALMemory;
        subscriberFunctions(); //implementation specific
    });
}

function disconnected(error) {
    console.log("disconnected");
}
QiSession(connected, disconnected, location.host);

function changeHeaderTheme(varHeaderTheme) {
    switch (varHeaderTheme) {
        case 1:
            $('.headerText').attr('style', 'color: #000');
            $('.btn.home').attr('style', 'background-image: url(./images/ht/header/btnHome_b.png');
            $('.btn.back').attr('style', 'display: none');
            break;
        case 2:
            $('.headerText').attr('style', 'color: #FFF');
            $('.btn.home').attr('style', 'background-image: url(./images/ht/header/btnHome_w.png');
            $('.btn.back').attr('style', 'display: none');
            break;
        case 3:
            $('.headerText').attr('style', 'color: #000');
            $('.btn.home').attr('style', 'background-image: url(./images/ht/header/btnHome_b.png');
            $('.btn.back').attr('style', 'background-image: url(./images/ht/header/btnBack_b.png');
            break;
        case 4:
            $('.headerText').attr('style', 'color: #FFF');
            $('.btn.home').attr('style', 'background-image: url(./images/ht/header/btnHome_w.png');
            $('.btn.back').attr('style', 'background-image: url(./images/ht/header/btnBack_w.png');
            break;
        case 5:
            $('.headerText').attr('style', 'color: #000');
            $('#header').attr('style', 'background-color: #1C9BD8');
            $('.btn.home').attr('style', 'background-image: url(./images/ht/header/btnHome_b.png');
            $('.btn.back').attr('style', 'display: none');
            break;
        case 6:
            $('.headerText').attr('style', 'color: #FFF');
            $('#header').attr('style', 'background-color: #1C9BD8');
            $('.btn.home').attr('style', 'background-image: url(./images/ht/header/btnHome_w.png');
            $('.btn.back').attr('style', 'display: none');
            break;
        case 7:
            $('.headerText').attr('style', 'color: #000');
            $('#header').attr('style', 'background-color: #1C9BD8');
            $('.btn.home').attr('style', 'background-image: url(./images/ht/header/btnHome_b.png');
            $('.btn.back').attr('style', 'background-image: url(./images/ht/header/btnBack_b.png');
            break;
        case 8:
            $('.headerText').attr('style', 'color: #FFF');
            $('#header').attr('style', 'background-color: #1C9BD8');
            $('.btn.home').attr('style', 'background-image: url(./images/ht/header/btnHome_w.png');
            $('.btn.back').attr('style', 'background-image: url(./images/ht/header/btnBack_w.png');
            break;
        default:
            break;
            //code block
    }
}

function calculateBubbleSizes(varNumOfBubbles) {
    switch (varNumOfBubbles) {
        case 1:
            $('#topContent').attr('style', 'left: 0%');
            $('.bubbles').attr('style', 'width: 500px');
            break;
        case 2:
            $('#topContent').attr('style', 'left: 0%');
            $('.bubbles').attr('style', 'width: 45%');
            break;
        case 3:
            $('#topContent').attr('style', 'left: 0%');
            $('.bubbles').attr('style', 'width: 324px');
            break;
        case 4:
            $('#topContent').attr('style', 'left: 0%');
            $('.bubbles').attr('style', 'width: 245px');
            break;
        case 5:
            $('#topContent').attr('style', 'left: 16%');
            $('.bubbles').attr('style', 'width: 245px');
            break;
        case 6:
            $('#topContent').attr('style', 'left: 0%');
            $('.bubbles').attr('style', 'width: 245px');
            break;
        case 7:
            $('#topContent').attr('style', 'left: 12%');
            $('.bubbles').attr('style', 'width: 245px');
            break;
        case 8:
            $('#topContent').attr('style', 'left: 0%');
            $('.bubbles').attr('style', 'width: 245px');
            break;
        default:
            break;
            //code block
    }
}

function button(id) {
    if (preventMultipleClick(id)) return; // prevend multiple clicks
    session.service("ALMemory").then(function (memory) {
        memory.raiseEvent(id, true);
    }, function (error) {
        console.log("An error occurred:", error);
    });
}

function goHome() {
    memory.raiseEvent('onSendHome', true).then(function () {
        console.log("home send");
    });
}
$(document).ready(function () {
    $("#home").click(function () {
        //textToSpeech._stopAll(true);
        goHome();
    });
    $(".back").click(function () {
        //textToSpeech._stopAll(true);
        //goHome();
    });
});

function subscriberFunctions() {
    memory.subscriber("onShowImage").then(function (subscriber) {
        subscriber.signal.connect(function (state) {
            console.log("image url" + state);
            result = state.search("http");
            if (result != 0) state = state.substr(1);
            $('#inlayImage').css('background-image', 'url(' + state + ')');
        });
    });
}
/*###*/
/*### HELPER FUNCTIONS*/
/*###*/
// MULTIPLE CLICK TO AVOID FAST CLICKS TO BE COUNTED SEVERAL TIMES
var multipleClick_last_clickTime = null;
var multipleClick_last_target = null;

function preventMultipleClick(id) {
    var preventClick = false;
    var currentClickTime = new Date();
    if (currentClickTime - multipleClick_last_clickTime < 500 && id == multipleClick_last_target) {
        preventClick = true;
    }
    multipleClick_last_target = id;
    multipleClick_last_clickTime = currentClickTime;
    return preventClick;
}
viewport = document.querySelector("meta[name=viewport]");
if (viewport != null) {
    var legacyWidth = 1280;
    var windowWidth = window.screen.width;
    var scale = (windowWidth / legacyWidth).toFixed(3);
    init_str = "initial-scale=".concat(scale.toString());
    min_str = "minimum-scale=".concat(scale.toString());
    max_str = "maximum-scale=".concat(scale.toString());
    viewport.setAttribute("content", init_str.concat(",").concat(min_str).concat(",").concat(max_str));
}

function passWord() {
    //setTimeout(function () {
    //    return " ";
    //}, 3000);
    var testV = 1;
    var pass1 = prompt('Bitte Passwort eingeben', ' ');
    while (testV < 3) {
        if (!pass1) history.go(-1);
        if (pass1.toLowerCase() == "1234") {
            button("onShowHiddenMenu");
            break;
        }
        testV += 1;
        var pass1 = prompt('Zugang verweigert - Passwort nicht korrekt, Bitte nochmal versuchen.', 'Passwort');
    }
    if (pass1.toLowerCase() != "password" & testV == 3) history.go(-1);
    return " ";
}
