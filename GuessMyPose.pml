<?xml version="1.0" encoding="UTF-8" ?>
<Package name="GuessMyPose" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="diaMain" src="diaMain/diaMain.dlg" />
    </Dialogs>
    <Resources>
        <File name="pepperfont-webfont" src="html/css/fonts/pepperfont-webfont.woff" />
        <File name="pepperfont-webfont" src="html/css/fonts/pepperfont-webfont.woff2" />
        <File name="main" src="html/css/main.css" />
        <File name="normalize.min" src="html/css/normalize.min.css" />
        <File name="posen" src="html/data/posen.js" />
        <File name="black" src="html/images/ht/black.png" />
        <File name="ht" src="html/images/ht/ht.jpg" />
        <File name="ht_icon" src="html/images/ht/ht_icon.png" />
        <File name="ht_logo" src="html/images/ht/ht_logo.png" />
        <File name="ht_logo_dark" src="html/images/ht/ht_logo_dark.png" />
        <File name="jquery.min" src="html/js/jquery.min.js" />
        <File name="main" src="html/js/main.js" />
        <File name="mainGMP" src="html/js/mainGMP.js" />
        <File name="gorilla" src="html/images/ht/posen/gorilla.jpg" />
        <File name="frage" src="html/frage.html" />
        <File name="frage" src="html/js/frage.js" />
        <File name="jquery-1.9.1.min" src="html/js/jquery-1.9.1.min.js" />
        <File name="jquery.autocomplete.min" src="html/js/jquery.autocomplete.min.js" />
        <File name="elephant" src="html/images/ht/posen/elephant.jpg" />
        <File name="laughing" src="html/images/ht/laughing.gif" />
        <File name="sad" src="html/images/ht/sad.gif" />
        <File name="mouse" src="html/images/ht/posen/mouse.jpg" />
        <File name="saxophone" src="html/images/ht/posen/saxophone.jpg" />
        <File name="vaccum" src="html/images/ht/posen/vaccum.jpg" />
        <File name="pepper" src="html/js/pepper.js" />
        <File name="0punkte" src="html/images/ht/0punkte.png" />
        <File name="1punkte" src="html/images/ht/1punkte.png" />
        <File name="2punkte" src="html/images/ht/2punkte.png" />
        <File name="3punkte" src="html/images/ht/3punkte.png" />
        <File name="End_Screen" src="html/images/ht/screens/End_Screen.png" />
        <File name="Game_01" src="html/images/ht/screens/Game_01.png" />
        <File name="Game_02 copy" src="html/images/ht/screens/Game_02 copy.png" />
        <File name="Game_02" src="html/images/ht/screens/Game_02.png" />
        <File name="Game_03" src="html/images/ht/screens/Game_03.png" />
        <File name="Start_Screen" src="html/images/ht/screens/Start_Screen.png" />
        <File name="start_screen" src="html/images/ht/start_screen.png" />
        <File name="leer" src="html/images/ht/leer.png" />
        <File name="posen_screen" src="html/images/ht/posen_screen.gif" />
        <File name="00" src="html/images/ht/00.png" />
        <File name="01" src="html/images/ht/01.png" />
        <File name="02" src="html/images/ht/02.png" />
        <File name="03" src="html/images/ht/03.png" />
        <File name="04" src="html/images/ht/04.png" />
        <File name="bandmaster" src="html/images/ht/posen/bandmaster.jpg" />
        <File name="disco" src="html/images/ht/posen/disco.jpg" />
        <File name="drivecar" src="html/images/ht/posen/drivecar.jpg" />
        <File name="football" src="html/images/ht/posen/football.jpg" />
        <File name="fotograph" src="html/images/ht/posen/fotograph.jpg" />
        <File name="golf" src="html/images/ht/posen/golf.jpg" />
        <File name="guitar" src="html/images/ht/posen/guitar.jpg" />
        <File name="knight" src="html/images/ht/posen/knight.jpg" />
        <File name="robot" src="html/images/ht/posen/robot.jpg" />
        <File name="showmuscles" src="html/images/ht/posen/showmuscles.jpg" />
        <File name="spaceshuttle" src="html/images/ht/posen/spaceshuttle.jpg" />
        <File name="taichi" src="html/images/ht/posen/taichi.jpg" />
        <File name="loveyou" src="html/images/ht/posen/loveyou.jpg" />
        <File name="btnBack_b" src="html/images/ht/header/btnBack_b.png" />
        <File name="btnBack_w" src="html/images/ht/header/btnBack_w.png" />
        <File name="btnHome_b" src="html/images/ht/header/btnHome_b.png" />
        <File name="btnHome_w" src="html/images/ht/header/btnHome_w.png" />
        <File name="empty_screen" src="html/images/ht/empty_screen.png" />
        <File name="end_screen" src="html/images/ht/end_screen.png" />
        <File name="monster" src="html/images/ht/posen/monster.jpg" />
        <File name="zombie" src="html/images/ht/posen/zombie.jpg" />
        <File name="batman" src="html/sounds/batman.ogg" />
    </Resources>
    <Topics>
        <Topic name="diaMain_ged" src="diaMain/diaMain_ged.top" topicName="diaMain" language="de_DE" />
        <Topic name="diaMain_enu" src="diaMain/diaMain_enu.top" topicName="diaMain" language="en_US" />
    </Topics>
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_de_DE" src="translations/translation_de_DE.ts" language="de_DE" />
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
